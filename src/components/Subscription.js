import React from "react";

export default class Subscription extends React.Component {
  static defaultProps = {
    url:
      "https://surge.us19.list-manage.com/subscribe/post?u=57a38bb36422b7a2af67c3a35&amp;id=2ab0997952",
    rss: "/rss.xml",
  };

  handleSubmit = () => {
    window.open(
      this.props.url,
      "popupwindow",
      "scrollbars=yes,width=800,height=600",
    );
    return true;
  };

  handleChange(event) {
    const node = event.target;
    const placeholder = node.getAttribute("placeholder");
    const { value } = event.target;
    if (value && value.length > placeholder.length) {
      node.setAttribute("size", value.length);
    }
  }

  render() {
    return (
      <header>
        <span style={{}}>
          <a href={this.props.rss}>РСС-лента</a>
        </span>
        <span className="middle">или подписаться через&nbsp;</span>
        <form
          className="subscribe"
          action={this.props.url}
          method="post"
          target="popupwindow"
          onSubmit={this.handleSubmit}
        >
          <input
            required="required"
            onChange={this.handleChange}
            size="6"
            placeholder="е-мейл"
            type="email"
            name="EMAIL"
            id="mce-EMAIL"
          />
          {" – "}
          <input type="submit" value="ок!" id="subscribe-button" />
        </form>
      </header>
    );
  }
}
