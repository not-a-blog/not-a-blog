---
title: Amsterdam food
---

- 🤷 05 августа: [Troost](https://goo.gl/maps/9Azi4tYqkaaGgDiu7). Бургеры и пиво.
- 👍 28 июля: [Hakata Senpachi](https://goo.gl/maps/vJGKYstB7pPafsii9). Японские шашлыки и куча видов выпивки.
- 👍 25 мая: [The Cottage](https://goo.gl/maps/H9sjbA1bX211tqECA). Английские яйца и бранч.
- 🤷 24 мая: [Libero](https://goo.gl/maps/LWk9wDCAMrmH86EY6). Неплохая лазанья, медленное обслуживание.
- 👍 23 мая: [Raming Kingdom](https://goo.gl/maps/4Bwso9x89uFMcbjf7). Рамен за барной стойкой от японцев, вся кухня приветствует тебя при входе.
- 🤷 22 мая: [Roses by SAL](https://goo.gl/maps/zZVRvAt72efrVj6Q9). Приятный садик в конце заведения, ужасная маргарита.
- 🤷 16 мая: [Box Sociaal](https://goo.gl/maps/dhuETbEJfoX3CYsT6). Макрель с картошкой и аспарагусом, среднего размера порция.
- 👍 15 мая: [Orontes](https://goo.gl/maps/cJNgKTH385ft7bxs6). Турецкое место со вкусным мясом.
- 👍 01 мая: [Tempura](https://goo.gl/maps/deCkecqY1k7bv11V9). Как всегда отлично и как всегда строгая хозяйка. Ел tempura salmon maki и beef udon.
- 👎 29 апреля: [Instock](https://goo.gl/maps/bQuyFprfCJBZXSqt5). Очень мелкие порции и очень долго, один официант на зал.
- 👎 25 апреля: [Bar Gallizia](https://goo.gl/maps/yfi96swfv7gGpLUc6). Недоваренный ризотто, туна слишком похожая на мясо.
- 👍 24 апреля: [Delirium](https://goo.gl/maps/XSbMgmCn3QNtxen68). Вкусный бургер, очень много разного пива.