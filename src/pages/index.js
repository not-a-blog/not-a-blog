import React from "react";
import Link from "gatsby-link";
import get from "lodash/get";
import Helmet from "react-helmet";

import Subscription from "../components/Subscription";

function title(node) {
  return get(node, "frontmatter.title") || node.fields.slug;
}

class BlogIndex extends React.Component {
  render() {
    const siteTitle = get(this, "props.data.site.siteMetadata.title");
    const posts = get(this, "props.data.allMarkdownRemark.edges");

    return (
      <div className="layout">
        <div className="layout__index">
          <h2>Не блог</h2>
          <Subscription />
          <Helmet title={siteTitle} />
          {posts.map(({ node }) => {
            const title = get(node, "frontmatter.title") || node.fields.slug;

            if (!get(node, "frontmatter.index")) {
              return null;
            }

            return (
              <div key={node.fields.slug} style={{ marginTop: "2em" }}>
                <h3>
                  <Link style={{ boxShadow: "none" }} to={node.fields.slug}>
                    {title}
                  </Link>
                </h3>
                <p className="post-date">{node.frontmatter.date}</p>
              </div>
            );
          })}
        </div>
        <div className="layout__collections">
          <h2>Коллекции</h2>
          {Array.from(posts)
            .sort((a, b) => {
              return title(a.node).localeCompare(title(b.node));
            })
            .map(({ node }) => {
              if (node.fields.slug.indexOf("/collections/") === -1) {
                return null;
              }

              return (
                <div
                  key={node.fields.slug}
                  style={{ fontSize: "0.7em", fontWeight: "bold" }}
                >
                  <Link style={{ boxShadow: "none" }} to={node.fields.slug}>
                    {title(node)}
                  </Link>
                </div>
              );
            })}
        </div>
      </div>
    );
  }
}

export default BlogIndex;

export const pageQuery = graphql`
  query IndexQuery {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "DD MMMM YYYY", locale: "ru")
            title
            index
          }
        }
      }
    }
  }
`;
