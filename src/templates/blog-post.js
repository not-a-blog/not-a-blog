import React from "react";
import Helmet from "react-helmet";
import get from "lodash/get";

import "../style/index.css";

class BlogPostTemplate extends React.Component {
  render() {
    const post = this.props.data.markdownRemark;

    const siteTitle = get(this.props, "data.site.siteMetadata.title");
    const { previous, next } = this.props.pathContext;

    return (
      <div>
        <Helmet
          title={
            post.frontmatter.title
              ? `${post.frontmatter.title} | ${siteTitle}`
              : `${siteTitle}`
          }
        />
        <h1>{post.frontmatter.title}</h1>
        {post.frontmatter.date && (
          <p className="post-date">{post.frontmatter.date}</p>
        )}
        <div className="post" dangerouslySetInnerHTML={{ __html: post.html }} />
      </div>
    );
  }
}

export default BlogPostTemplate;

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      html
      frontmatter {
        title
        date(formatString: "DD MMMM YYYY", locale: "ru")
      }
    }
  }
`;
