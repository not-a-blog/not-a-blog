---
title: Movies
---

Очень персональный список любимых фильмов.

## Фильмы

- [Mullholand Dr.](https://www.imdb.com/title/tt0166924)
- [Lost in Translation](https://www.imdb.com/title/tt0335266)
- [Donnie Darko](https://www.imdb.com/title/tt0246578)
- [Magnolia](https://www.imdb.com/title/tt0175880)
- [The Tenant](https://www.imdb.com/title/tt0074811)
- [Synecdoche, New York](https://www.imdb.com/title/tt0383028)
- [True Romance](https://www.imdb.com/title/tt0108399)
- [Gattaca](https://www.imdb.com/title/tt0119177)
- [The Royal Tenenbaums](https://www.imdb.com/title/tt0265666)
- [Fargo](https://www.imdb.com/title/tt0116282)
- [The Deer Hunter](https://www.imdb.com/title/tt0077416)
- [Eastern Promises](https://www.imdb.com/title/tt0765443)
- [Who's Afraid of Virginia Woolf?](https://www.imdb.com/title/tt0061184)
- [Strangers on a Train](https://www.imdb.com/title/tt0044079)
- [Her](https://www.imdb.com/title/tt1798709)
- [Melancholia](https://www.imdb.com/title/tt1527186)
- [Москва](http://www.imdb.com/title/tt0160550/)
- [Wonder Boys](https://www.imdb.com/title/tt0185014)
- [The Ghost Writer](https://www.imdb.com/title/tt1139328)
- [La règle du jeu](https://www.imdb.com/title/tt0031885)
- [La montaña sagrada](https://www.imdb.com/title/tt0071615)
- [Birdman or (The Unexpected Virtue of Ignorance)](https://www.imdb.com/title/tt2562232)
- [Deconstructing Harry](https://www.imdb.com/title/tt0118954)
- [Social Network](https://www.imdb.com/title/tt1285016)
- [Porco Rosso](https://www.imdb.com/title/tt0104652)

## Телевизор

- [Friends](https://www.imdb.com/title/tt0108778)
- [Twin Peaks](https://www.imdb.com/title/tt0098936)
- [Arrested Development](https://www.imdb.com/title/tt0367279)
- [Weeds](https://www.imdb.com/title/tt0439100)
- [Community](https://www.imdb.com/title/tt1439629)

### Где искать кино?

- [rogerebert.com](https://www.rogerebert.com/)
- [Criterion Collection](https://www.criterion.com/)
- [LaCinetek](https://www.lacinetek.com)
- [LetterBoxd](https://letterboxd.com)

### IMDB

https://www.imdb.com/user/ur23913049/ratings
