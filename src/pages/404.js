import React from "react";

export default class NotFound extends React.Component {
  render() {
    return <p>Нет такой страницы :-(</p>;
  }
}
