import React from "react";
import Link from "gatsby-link";

class Template extends React.Component {
  render() {
    const { location, children } = this.props;
    let header;
    if (
      location.pathname === "/" ||
      location.pathname.startsWith("/subscription-success")
    ) {
      header = null;
    } else {
      header = (
        <h3>
          <Link to={"/"}>←</Link>
        </h3>
      );
    }
    return (
      <div className="container">
        {header}
        {children()}
      </div>
    );
  }
}

Template.propTypes = {
  children: React.PropTypes.func,
  location: React.PropTypes.object,
  route: React.PropTypes.object,
};

export default Template;
